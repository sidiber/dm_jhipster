export enum Role {
  COURSIER = 'COURSIER',

  COMMERCANT = 'COMMERCANT',

  CLIENT = 'CLIENT',
}
