import { ICompte } from 'app/entities/compte/compte.model';
import { ISystemePaiement } from 'app/entities/systeme-paiement/systeme-paiement.model';

export interface IPanier {
  id?: number;
  prixTotal?: string | null;
  constituePar?: ICompte | null;
  estValidePar?: ISystemePaiement | null;
}

export class Panier implements IPanier {
  constructor(
    public id?: number,
    public prixTotal?: string | null,
    public constituePar?: ICompte | null,
    public estValidePar?: ISystemePaiement | null
  ) {}
}

export function getPanierIdentifier(panier: IPanier): number | undefined {
  return panier.id;
}
