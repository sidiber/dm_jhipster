import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SystemePaiementService } from 'app/entities/systeme-paiement/systeme-paiement.service';
import { ISystemePaiement, SystemePaiement } from 'app/shared/model/systeme-paiement.model';

describe('Service Tests', () => {
  describe('SystemePaiement Service', () => {
    let injector: TestBed;
    let service: SystemePaiementService;
    let httpMock: HttpTestingController;
    let elemDefault: ISystemePaiement;
    let expectedResult: ISystemePaiement | ISystemePaiement[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(SystemePaiementService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new SystemePaiement(0, 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA', currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateExpiration: currentDate.format(DATE_TIME_FORMAT),
            dateFacture: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a SystemePaiement', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateExpiration: currentDate.format(DATE_TIME_FORMAT),
            dateFacture: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateExpiration: currentDate,
            dateFacture: currentDate,
          },
          returnedFromService
        );

        service.create(new SystemePaiement()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a SystemePaiement', () => {
        const returnedFromService = Object.assign(
          {
            numCarte: 'BBBBBB',
            dateExpiration: currentDate.format(DATE_TIME_FORMAT),
            typeCarte: 'BBBBBB',
            montant: 'BBBBBB',
            dateFacture: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateExpiration: currentDate,
            dateFacture: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of SystemePaiement', () => {
        const returnedFromService = Object.assign(
          {
            numCarte: 'BBBBBB',
            dateExpiration: currentDate.format(DATE_TIME_FORMAT),
            typeCarte: 'BBBBBB',
            montant: 'BBBBBB',
            dateFacture: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateExpiration: currentDate,
            dateFacture: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SystemePaiement', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
