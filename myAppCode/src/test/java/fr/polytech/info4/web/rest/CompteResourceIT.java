package fr.polytech.info4.web.rest;

import fr.polytech.info4.CoopcycleApp;
import fr.polytech.info4.domain.Compte;
import fr.polytech.info4.domain.Panier;
import fr.polytech.info4.domain.Course;
import fr.polytech.info4.domain.Cooperative;
import fr.polytech.info4.repository.CompteRepository;
import fr.polytech.info4.service.CompteService;
import fr.polytech.info4.service.dto.CompteDTO;
import fr.polytech.info4.service.mapper.CompteMapper;
import fr.polytech.info4.service.dto.CompteCriteria;
import fr.polytech.info4.service.CompteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.polytech.info4.domain.enumeration.Role;
/**
 * Integration tests for the {@link CompteResource} REST controller.
 */
@SpringBootTest(classes = CoopcycleApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompteResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "3Fii@univ-grenoble-alpess.com";
    private static final String UPDATED_EMAIL = "i.oy@hotmail.fr";

    private static final Role DEFAULT_CATEGORIE = Role.COURSIER;
    private static final Role UPDATED_CATEGORIE = Role.COMMERCANT;

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_COMPTE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_COMPTE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_P_COMPTE = "AAAAA";
    private static final String UPDATED_CODE_P_COMPTE = "BBBBB";

    private static final String DEFAULT_VILLE_COMPTE = "AAAAAAAAAA";
    private static final String UPDATED_VILLE_COMPTE = "BBBBBBBBBB";

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private CompteMapper compteMapper;

    @Autowired
    private CompteService compteService;

    @Autowired
    private CompteQueryService compteQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompteMockMvc;

    private Compte compte;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Compte createEntity(EntityManager em) {
        Compte compte = new Compte()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .email(DEFAULT_EMAIL)
            .categorie(DEFAULT_CATEGORIE)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .addressCompte(DEFAULT_ADDRESS_COMPTE)
            .codePCompte(DEFAULT_CODE_P_COMPTE)
            .villeCompte(DEFAULT_VILLE_COMPTE);
        return compte;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Compte createUpdatedEntity(EntityManager em) {
        Compte compte = new Compte()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .categorie(UPDATED_CATEGORIE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .addressCompte(UPDATED_ADDRESS_COMPTE)
            .codePCompte(UPDATED_CODE_P_COMPTE)
            .villeCompte(UPDATED_VILLE_COMPTE);
        return compte;
    }

    @BeforeEach
    public void initTest() {
        compte = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompte() throws Exception {
        int databaseSizeBeforeCreate = compteRepository.findAll().size();
        // Create the Compte
        CompteDTO compteDTO = compteMapper.toDto(compte);
        restCompteMockMvc.perform(post("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isCreated());

        // Validate the Compte in the database
        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeCreate + 1);
        Compte testCompte = compteList.get(compteList.size() - 1);
        assertThat(testCompte.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCompte.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testCompte.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCompte.getCategorie()).isEqualTo(DEFAULT_CATEGORIE);
        assertThat(testCompte.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testCompte.getAddressCompte()).isEqualTo(DEFAULT_ADDRESS_COMPTE);
        assertThat(testCompte.getCodePCompte()).isEqualTo(DEFAULT_CODE_P_COMPTE);
        assertThat(testCompte.getVilleCompte()).isEqualTo(DEFAULT_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void createCompteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = compteRepository.findAll().size();

        // Create the Compte with an existing ID
        compte.setId(1L);
        CompteDTO compteDTO = compteMapper.toDto(compte);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompteMockMvc.perform(post("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Compte in the database
        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = compteRepository.findAll().size();
        // set the field null
        compte.setNom(null);

        // Create the Compte, which fails.
        CompteDTO compteDTO = compteMapper.toDto(compte);


        restCompteMockMvc.perform(post("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isBadRequest());

        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = compteRepository.findAll().size();
        // set the field null
        compte.setPrenom(null);

        // Create the Compte, which fails.
        CompteDTO compteDTO = compteMapper.toDto(compte);


        restCompteMockMvc.perform(post("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isBadRequest());

        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = compteRepository.findAll().size();
        // set the field null
        compte.setEmail(null);

        // Create the Compte, which fails.
        CompteDTO compteDTO = compteMapper.toDto(compte);


        restCompteMockMvc.perform(post("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isBadRequest());

        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllComptes() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList
        restCompteMockMvc.perform(get("/api/comptes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compte.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].categorie").value(hasItem(DEFAULT_CATEGORIE.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].addressCompte").value(hasItem(DEFAULT_ADDRESS_COMPTE)))
            .andExpect(jsonPath("$.[*].codePCompte").value(hasItem(DEFAULT_CODE_P_COMPTE)))
            .andExpect(jsonPath("$.[*].villeCompte").value(hasItem(DEFAULT_VILLE_COMPTE)));
    }
    
    @Test
    @Transactional
    public void getCompte() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get the compte
        restCompteMockMvc.perform(get("/api/comptes/{id}", compte.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compte.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.categorie").value(DEFAULT_CATEGORIE.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.addressCompte").value(DEFAULT_ADDRESS_COMPTE))
            .andExpect(jsonPath("$.codePCompte").value(DEFAULT_CODE_P_COMPTE))
            .andExpect(jsonPath("$.villeCompte").value(DEFAULT_VILLE_COMPTE));
    }


    @Test
    @Transactional
    public void getComptesByIdFiltering() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        Long id = compte.getId();

        defaultCompteShouldBeFound("id.equals=" + id);
        defaultCompteShouldNotBeFound("id.notEquals=" + id);

        defaultCompteShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompteShouldNotBeFound("id.greaterThan=" + id);

        defaultCompteShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompteShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllComptesByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom equals to DEFAULT_NOM
        defaultCompteShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the compteList where nom equals to UPDATED_NOM
        defaultCompteShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllComptesByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom not equals to DEFAULT_NOM
        defaultCompteShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the compteList where nom not equals to UPDATED_NOM
        defaultCompteShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllComptesByNomIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultCompteShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the compteList where nom equals to UPDATED_NOM
        defaultCompteShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllComptesByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom is not null
        defaultCompteShouldBeFound("nom.specified=true");

        // Get all the compteList where nom is null
        defaultCompteShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByNomContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom contains DEFAULT_NOM
        defaultCompteShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the compteList where nom contains UPDATED_NOM
        defaultCompteShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllComptesByNomNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where nom does not contain DEFAULT_NOM
        defaultCompteShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the compteList where nom does not contain UPDATED_NOM
        defaultCompteShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllComptesByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom equals to DEFAULT_PRENOM
        defaultCompteShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the compteList where prenom equals to UPDATED_PRENOM
        defaultCompteShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllComptesByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom not equals to DEFAULT_PRENOM
        defaultCompteShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the compteList where prenom not equals to UPDATED_PRENOM
        defaultCompteShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllComptesByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultCompteShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the compteList where prenom equals to UPDATED_PRENOM
        defaultCompteShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllComptesByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom is not null
        defaultCompteShouldBeFound("prenom.specified=true");

        // Get all the compteList where prenom is null
        defaultCompteShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByPrenomContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom contains DEFAULT_PRENOM
        defaultCompteShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the compteList where prenom contains UPDATED_PRENOM
        defaultCompteShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllComptesByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where prenom does not contain DEFAULT_PRENOM
        defaultCompteShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the compteList where prenom does not contain UPDATED_PRENOM
        defaultCompteShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllComptesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email equals to DEFAULT_EMAIL
        defaultCompteShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the compteList where email equals to UPDATED_EMAIL
        defaultCompteShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllComptesByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email not equals to DEFAULT_EMAIL
        defaultCompteShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the compteList where email not equals to UPDATED_EMAIL
        defaultCompteShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllComptesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultCompteShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the compteList where email equals to UPDATED_EMAIL
        defaultCompteShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllComptesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email is not null
        defaultCompteShouldBeFound("email.specified=true");

        // Get all the compteList where email is null
        defaultCompteShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByEmailContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email contains DEFAULT_EMAIL
        defaultCompteShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the compteList where email contains UPDATED_EMAIL
        defaultCompteShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllComptesByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where email does not contain DEFAULT_EMAIL
        defaultCompteShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the compteList where email does not contain UPDATED_EMAIL
        defaultCompteShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllComptesByCategorieIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where categorie equals to DEFAULT_CATEGORIE
        defaultCompteShouldBeFound("categorie.equals=" + DEFAULT_CATEGORIE);

        // Get all the compteList where categorie equals to UPDATED_CATEGORIE
        defaultCompteShouldNotBeFound("categorie.equals=" + UPDATED_CATEGORIE);
    }

    @Test
    @Transactional
    public void getAllComptesByCategorieIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where categorie not equals to DEFAULT_CATEGORIE
        defaultCompteShouldNotBeFound("categorie.notEquals=" + DEFAULT_CATEGORIE);

        // Get all the compteList where categorie not equals to UPDATED_CATEGORIE
        defaultCompteShouldBeFound("categorie.notEquals=" + UPDATED_CATEGORIE);
    }

    @Test
    @Transactional
    public void getAllComptesByCategorieIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where categorie in DEFAULT_CATEGORIE or UPDATED_CATEGORIE
        defaultCompteShouldBeFound("categorie.in=" + DEFAULT_CATEGORIE + "," + UPDATED_CATEGORIE);

        // Get all the compteList where categorie equals to UPDATED_CATEGORIE
        defaultCompteShouldNotBeFound("categorie.in=" + UPDATED_CATEGORIE);
    }

    @Test
    @Transactional
    public void getAllComptesByCategorieIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where categorie is not null
        defaultCompteShouldBeFound("categorie.specified=true");

        // Get all the compteList where categorie is null
        defaultCompteShouldNotBeFound("categorie.specified=false");
    }

    @Test
    @Transactional
    public void getAllComptesByPhoneNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber equals to DEFAULT_PHONE_NUMBER
        defaultCompteShouldBeFound("phoneNumber.equals=" + DEFAULT_PHONE_NUMBER);

        // Get all the compteList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultCompteShouldNotBeFound("phoneNumber.equals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllComptesByPhoneNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber not equals to DEFAULT_PHONE_NUMBER
        defaultCompteShouldNotBeFound("phoneNumber.notEquals=" + DEFAULT_PHONE_NUMBER);

        // Get all the compteList where phoneNumber not equals to UPDATED_PHONE_NUMBER
        defaultCompteShouldBeFound("phoneNumber.notEquals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllComptesByPhoneNumberIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber in DEFAULT_PHONE_NUMBER or UPDATED_PHONE_NUMBER
        defaultCompteShouldBeFound("phoneNumber.in=" + DEFAULT_PHONE_NUMBER + "," + UPDATED_PHONE_NUMBER);

        // Get all the compteList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultCompteShouldNotBeFound("phoneNumber.in=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllComptesByPhoneNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber is not null
        defaultCompteShouldBeFound("phoneNumber.specified=true");

        // Get all the compteList where phoneNumber is null
        defaultCompteShouldNotBeFound("phoneNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByPhoneNumberContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber contains DEFAULT_PHONE_NUMBER
        defaultCompteShouldBeFound("phoneNumber.contains=" + DEFAULT_PHONE_NUMBER);

        // Get all the compteList where phoneNumber contains UPDATED_PHONE_NUMBER
        defaultCompteShouldNotBeFound("phoneNumber.contains=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllComptesByPhoneNumberNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where phoneNumber does not contain DEFAULT_PHONE_NUMBER
        defaultCompteShouldNotBeFound("phoneNumber.doesNotContain=" + DEFAULT_PHONE_NUMBER);

        // Get all the compteList where phoneNumber does not contain UPDATED_PHONE_NUMBER
        defaultCompteShouldBeFound("phoneNumber.doesNotContain=" + UPDATED_PHONE_NUMBER);
    }


    @Test
    @Transactional
    public void getAllComptesByAddressCompteIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte equals to DEFAULT_ADDRESS_COMPTE
        defaultCompteShouldBeFound("addressCompte.equals=" + DEFAULT_ADDRESS_COMPTE);

        // Get all the compteList where addressCompte equals to UPDATED_ADDRESS_COMPTE
        defaultCompteShouldNotBeFound("addressCompte.equals=" + UPDATED_ADDRESS_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByAddressCompteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte not equals to DEFAULT_ADDRESS_COMPTE
        defaultCompteShouldNotBeFound("addressCompte.notEquals=" + DEFAULT_ADDRESS_COMPTE);

        // Get all the compteList where addressCompte not equals to UPDATED_ADDRESS_COMPTE
        defaultCompteShouldBeFound("addressCompte.notEquals=" + UPDATED_ADDRESS_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByAddressCompteIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte in DEFAULT_ADDRESS_COMPTE or UPDATED_ADDRESS_COMPTE
        defaultCompteShouldBeFound("addressCompte.in=" + DEFAULT_ADDRESS_COMPTE + "," + UPDATED_ADDRESS_COMPTE);

        // Get all the compteList where addressCompte equals to UPDATED_ADDRESS_COMPTE
        defaultCompteShouldNotBeFound("addressCompte.in=" + UPDATED_ADDRESS_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByAddressCompteIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte is not null
        defaultCompteShouldBeFound("addressCompte.specified=true");

        // Get all the compteList where addressCompte is null
        defaultCompteShouldNotBeFound("addressCompte.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByAddressCompteContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte contains DEFAULT_ADDRESS_COMPTE
        defaultCompteShouldBeFound("addressCompte.contains=" + DEFAULT_ADDRESS_COMPTE);

        // Get all the compteList where addressCompte contains UPDATED_ADDRESS_COMPTE
        defaultCompteShouldNotBeFound("addressCompte.contains=" + UPDATED_ADDRESS_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByAddressCompteNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where addressCompte does not contain DEFAULT_ADDRESS_COMPTE
        defaultCompteShouldNotBeFound("addressCompte.doesNotContain=" + DEFAULT_ADDRESS_COMPTE);

        // Get all the compteList where addressCompte does not contain UPDATED_ADDRESS_COMPTE
        defaultCompteShouldBeFound("addressCompte.doesNotContain=" + UPDATED_ADDRESS_COMPTE);
    }


    @Test
    @Transactional
    public void getAllComptesByCodePCompteIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte equals to DEFAULT_CODE_P_COMPTE
        defaultCompteShouldBeFound("codePCompte.equals=" + DEFAULT_CODE_P_COMPTE);

        // Get all the compteList where codePCompte equals to UPDATED_CODE_P_COMPTE
        defaultCompteShouldNotBeFound("codePCompte.equals=" + UPDATED_CODE_P_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByCodePCompteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte not equals to DEFAULT_CODE_P_COMPTE
        defaultCompteShouldNotBeFound("codePCompte.notEquals=" + DEFAULT_CODE_P_COMPTE);

        // Get all the compteList where codePCompte not equals to UPDATED_CODE_P_COMPTE
        defaultCompteShouldBeFound("codePCompte.notEquals=" + UPDATED_CODE_P_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByCodePCompteIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte in DEFAULT_CODE_P_COMPTE or UPDATED_CODE_P_COMPTE
        defaultCompteShouldBeFound("codePCompte.in=" + DEFAULT_CODE_P_COMPTE + "," + UPDATED_CODE_P_COMPTE);

        // Get all the compteList where codePCompte equals to UPDATED_CODE_P_COMPTE
        defaultCompteShouldNotBeFound("codePCompte.in=" + UPDATED_CODE_P_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByCodePCompteIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte is not null
        defaultCompteShouldBeFound("codePCompte.specified=true");

        // Get all the compteList where codePCompte is null
        defaultCompteShouldNotBeFound("codePCompte.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByCodePCompteContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte contains DEFAULT_CODE_P_COMPTE
        defaultCompteShouldBeFound("codePCompte.contains=" + DEFAULT_CODE_P_COMPTE);

        // Get all the compteList where codePCompte contains UPDATED_CODE_P_COMPTE
        defaultCompteShouldNotBeFound("codePCompte.contains=" + UPDATED_CODE_P_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByCodePCompteNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where codePCompte does not contain DEFAULT_CODE_P_COMPTE
        defaultCompteShouldNotBeFound("codePCompte.doesNotContain=" + DEFAULT_CODE_P_COMPTE);

        // Get all the compteList where codePCompte does not contain UPDATED_CODE_P_COMPTE
        defaultCompteShouldBeFound("codePCompte.doesNotContain=" + UPDATED_CODE_P_COMPTE);
    }


    @Test
    @Transactional
    public void getAllComptesByVilleCompteIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte equals to DEFAULT_VILLE_COMPTE
        defaultCompteShouldBeFound("villeCompte.equals=" + DEFAULT_VILLE_COMPTE);

        // Get all the compteList where villeCompte equals to UPDATED_VILLE_COMPTE
        defaultCompteShouldNotBeFound("villeCompte.equals=" + UPDATED_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByVilleCompteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte not equals to DEFAULT_VILLE_COMPTE
        defaultCompteShouldNotBeFound("villeCompte.notEquals=" + DEFAULT_VILLE_COMPTE);

        // Get all the compteList where villeCompte not equals to UPDATED_VILLE_COMPTE
        defaultCompteShouldBeFound("villeCompte.notEquals=" + UPDATED_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByVilleCompteIsInShouldWork() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte in DEFAULT_VILLE_COMPTE or UPDATED_VILLE_COMPTE
        defaultCompteShouldBeFound("villeCompte.in=" + DEFAULT_VILLE_COMPTE + "," + UPDATED_VILLE_COMPTE);

        // Get all the compteList where villeCompte equals to UPDATED_VILLE_COMPTE
        defaultCompteShouldNotBeFound("villeCompte.in=" + UPDATED_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByVilleCompteIsNullOrNotNull() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte is not null
        defaultCompteShouldBeFound("villeCompte.specified=true");

        // Get all the compteList where villeCompte is null
        defaultCompteShouldNotBeFound("villeCompte.specified=false");
    }
                @Test
    @Transactional
    public void getAllComptesByVilleCompteContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte contains DEFAULT_VILLE_COMPTE
        defaultCompteShouldBeFound("villeCompte.contains=" + DEFAULT_VILLE_COMPTE);

        // Get all the compteList where villeCompte contains UPDATED_VILLE_COMPTE
        defaultCompteShouldNotBeFound("villeCompte.contains=" + UPDATED_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void getAllComptesByVilleCompteNotContainsSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        // Get all the compteList where villeCompte does not contain DEFAULT_VILLE_COMPTE
        defaultCompteShouldNotBeFound("villeCompte.doesNotContain=" + DEFAULT_VILLE_COMPTE);

        // Get all the compteList where villeCompte does not contain UPDATED_VILLE_COMPTE
        defaultCompteShouldBeFound("villeCompte.doesNotContain=" + UPDATED_VILLE_COMPTE);
    }


    @Test
    @Transactional
    public void getAllComptesByPanierIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);
        Panier panier = PanierResourceIT.createEntity(em);
        em.persist(panier);
        em.flush();
        compte.addPanier(panier);
        compteRepository.saveAndFlush(compte);
        Long panierId = panier.getId();

        // Get all the compteList where panier equals to panierId
        defaultCompteShouldBeFound("panierId.equals=" + panierId);

        // Get all the compteList where panier equals to panierId + 1
        defaultCompteShouldNotBeFound("panierId.equals=" + (panierId + 1));
    }


    @Test
    @Transactional
    public void getAllComptesByCourseIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);
        Course course = CourseResourceIT.createEntity(em);
        em.persist(course);
        em.flush();
        compte.addCourse(course);
        compteRepository.saveAndFlush(compte);
        Long courseId = course.getId();

        // Get all the compteList where course equals to courseId
        defaultCompteShouldBeFound("courseId.equals=" + courseId);

        // Get all the compteList where course equals to courseId + 1
        defaultCompteShouldNotBeFound("courseId.equals=" + (courseId + 1));
    }


    @Test
    @Transactional
    public void getAllComptesByMembreDeIsEqualToSomething() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);
        Cooperative membreDe = CooperativeResourceIT.createEntity(em);
        em.persist(membreDe);
        em.flush();
        compte.setMembreDe(membreDe);
        compteRepository.saveAndFlush(compte);
        Long membreDeId = membreDe.getId();

        // Get all the compteList where membreDe equals to membreDeId
        defaultCompteShouldBeFound("membreDeId.equals=" + membreDeId);

        // Get all the compteList where membreDe equals to membreDeId + 1
        defaultCompteShouldNotBeFound("membreDeId.equals=" + (membreDeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompteShouldBeFound(String filter) throws Exception {
        restCompteMockMvc.perform(get("/api/comptes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compte.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].categorie").value(hasItem(DEFAULT_CATEGORIE.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].addressCompte").value(hasItem(DEFAULT_ADDRESS_COMPTE)))
            .andExpect(jsonPath("$.[*].codePCompte").value(hasItem(DEFAULT_CODE_P_COMPTE)))
            .andExpect(jsonPath("$.[*].villeCompte").value(hasItem(DEFAULT_VILLE_COMPTE)));

        // Check, that the count call also returns 1
        restCompteMockMvc.perform(get("/api/comptes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompteShouldNotBeFound(String filter) throws Exception {
        restCompteMockMvc.perform(get("/api/comptes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompteMockMvc.perform(get("/api/comptes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompte() throws Exception {
        // Get the compte
        restCompteMockMvc.perform(get("/api/comptes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompte() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        int databaseSizeBeforeUpdate = compteRepository.findAll().size();

        // Update the compte
        Compte updatedCompte = compteRepository.findById(compte.getId()).get();
        // Disconnect from session so that the updates on updatedCompte are not directly saved in db
        em.detach(updatedCompte);
        updatedCompte
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .categorie(UPDATED_CATEGORIE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .addressCompte(UPDATED_ADDRESS_COMPTE)
            .codePCompte(UPDATED_CODE_P_COMPTE)
            .villeCompte(UPDATED_VILLE_COMPTE);
        CompteDTO compteDTO = compteMapper.toDto(updatedCompte);

        restCompteMockMvc.perform(put("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isOk());

        // Validate the Compte in the database
        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeUpdate);
        Compte testCompte = compteList.get(compteList.size() - 1);
        assertThat(testCompte.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCompte.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testCompte.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCompte.getCategorie()).isEqualTo(UPDATED_CATEGORIE);
        assertThat(testCompte.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testCompte.getAddressCompte()).isEqualTo(UPDATED_ADDRESS_COMPTE);
        assertThat(testCompte.getCodePCompte()).isEqualTo(UPDATED_CODE_P_COMPTE);
        assertThat(testCompte.getVilleCompte()).isEqualTo(UPDATED_VILLE_COMPTE);
    }

    @Test
    @Transactional
    public void updateNonExistingCompte() throws Exception {
        int databaseSizeBeforeUpdate = compteRepository.findAll().size();

        // Create the Compte
        CompteDTO compteDTO = compteMapper.toDto(compte);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompteMockMvc.perform(put("/api/comptes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Compte in the database
        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompte() throws Exception {
        // Initialize the database
        compteRepository.saveAndFlush(compte);

        int databaseSizeBeforeDelete = compteRepository.findAll().size();

        // Delete the compte
        restCompteMockMvc.perform(delete("/api/comptes/{id}", compte.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Compte> compteList = compteRepository.findAll();
        assertThat(compteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
