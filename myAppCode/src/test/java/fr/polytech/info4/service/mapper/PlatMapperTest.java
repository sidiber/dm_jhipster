package fr.polytech.info4.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PlatMapperTest {

    private PlatMapper platMapper;

    @BeforeEach
    public void setUp() {
        platMapper = new PlatMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(platMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(platMapper.fromId(null)).isNull();
    }
}
