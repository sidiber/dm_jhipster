package fr.polytech.info4.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.polytech.info4.web.rest.TestUtil;

public class PlatDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlatDTO.class);
        PlatDTO platDTO1 = new PlatDTO();
        platDTO1.setId(1L);
        PlatDTO platDTO2 = new PlatDTO();
        assertThat(platDTO1).isNotEqualTo(platDTO2);
        platDTO2.setId(platDTO1.getId());
        assertThat(platDTO1).isEqualTo(platDTO2);
        platDTO2.setId(2L);
        assertThat(platDTO1).isNotEqualTo(platDTO2);
        platDTO1.setId(null);
        assertThat(platDTO1).isNotEqualTo(platDTO2);
    }
}
