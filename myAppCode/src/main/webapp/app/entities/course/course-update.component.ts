import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICourse, Course } from 'app/shared/model/course.model';
import { CourseService } from './course.service';
import { IPanier } from 'app/shared/model/panier.model';
import { PanierService } from 'app/entities/panier/panier.service';
import { IPlat } from 'app/shared/model/plat.model';
import { PlatService } from 'app/entities/plat/plat.service';
import { ICompte } from 'app/shared/model/compte.model';
import { CompteService } from 'app/entities/compte/compte.service';

type SelectableEntity = IPanier | IPlat | ICompte;

@Component({
  selector: 'jhi-course-update',
  templateUrl: './course-update.component.html',
})
export class CourseUpdateComponent implements OnInit {
  isSaving = false;
  montants: IPanier[] = [];
  plats: IPlat[] = [];
  comptes: ICompte[] = [];

  editForm = this.fb.group({
    id: [],
    createdAt: [null, [Validators.required]],
    etat: [],
    startTime: [null, [Validators.required]],
    endTime: [],
    montantId: [],
    plats: [],
    livreParId: [],
  });

  constructor(
    protected courseService: CourseService,
    protected panierService: PanierService,
    protected platService: PlatService,
    protected compteService: CompteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ course }) => {
      if (!course.id) {
        const today = moment().startOf('day');
        course.createdAt = today;
        course.startTime = today;
        course.endTime = today;
      }

      this.updateForm(course);

      this.panierService
        .query({ 'courseId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IPanier[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IPanier[]) => {
          if (!course.montantId) {
            this.montants = resBody;
          } else {
            this.panierService
              .find(course.montantId)
              .pipe(
                map((subRes: HttpResponse<IPanier>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IPanier[]) => (this.montants = concatRes));
          }
        });

      this.platService.query().subscribe((res: HttpResponse<IPlat[]>) => (this.plats = res.body || []));

      this.compteService.query().subscribe((res: HttpResponse<ICompte[]>) => (this.comptes = res.body || []));
    });
  }

  updateForm(course: ICourse): void {
    this.editForm.patchValue({
      id: course.id,
      createdAt: course.createdAt ? course.createdAt.format(DATE_TIME_FORMAT) : null,
      etat: course.etat,
      startTime: course.startTime ? course.startTime.format(DATE_TIME_FORMAT) : null,
      endTime: course.endTime ? course.endTime.format(DATE_TIME_FORMAT) : null,
      montantId: course.montantId,
      plats: course.plats,
      livreParId: course.livreParId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const course = this.createFromForm();
    if (course.id !== undefined) {
      this.subscribeToSaveResponse(this.courseService.update(course));
    } else {
      this.subscribeToSaveResponse(this.courseService.create(course));
    }
  }

  private createFromForm(): ICourse {
    return {
      ...new Course(),
      id: this.editForm.get(['id'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      etat: this.editForm.get(['etat'])!.value,
      startTime: this.editForm.get(['startTime'])!.value ? moment(this.editForm.get(['startTime'])!.value, DATE_TIME_FORMAT) : undefined,
      endTime: this.editForm.get(['endTime'])!.value ? moment(this.editForm.get(['endTime'])!.value, DATE_TIME_FORMAT) : undefined,
      montantId: this.editForm.get(['montantId'])!.value,
      plats: this.editForm.get(['plats'])!.value,
      livreParId: this.editForm.get(['livreParId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICourse>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IPlat[], option: IPlat): IPlat {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
