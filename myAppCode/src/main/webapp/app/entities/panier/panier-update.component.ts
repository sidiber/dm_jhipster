import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPanier, Panier } from 'app/shared/model/panier.model';
import { PanierService } from './panier.service';
import { ICompte } from 'app/shared/model/compte.model';
import { CompteService } from 'app/entities/compte/compte.service';
import { ISystemePaiement } from 'app/shared/model/systeme-paiement.model';
import { SystemePaiementService } from 'app/entities/systeme-paiement/systeme-paiement.service';

type SelectableEntity = ICompte | ISystemePaiement;

@Component({
  selector: 'jhi-panier-update',
  templateUrl: './panier-update.component.html',
})
export class PanierUpdateComponent implements OnInit {
  isSaving = false;
  comptes: ICompte[] = [];
  systemepaiements: ISystemePaiement[] = [];

  editForm = this.fb.group({
    id: [],
    prixTotal: [],
    constitueParId: [],
    estValideParId: [],
  });

  constructor(
    protected panierService: PanierService,
    protected compteService: CompteService,
    protected systemePaiementService: SystemePaiementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ panier }) => {
      this.updateForm(panier);

      this.compteService.query().subscribe((res: HttpResponse<ICompte[]>) => (this.comptes = res.body || []));

      this.systemePaiementService.query().subscribe((res: HttpResponse<ISystemePaiement[]>) => (this.systemepaiements = res.body || []));
    });
  }

  updateForm(panier: IPanier): void {
    this.editForm.patchValue({
      id: panier.id,
      prixTotal: panier.prixTotal,
      constitueParId: panier.constitueParId,
      estValideParId: panier.estValideParId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const panier = this.createFromForm();
    if (panier.id !== undefined) {
      this.subscribeToSaveResponse(this.panierService.update(panier));
    } else {
      this.subscribeToSaveResponse(this.panierService.create(panier));
    }
  }

  private createFromForm(): IPanier {
    return {
      ...new Panier(),
      id: this.editForm.get(['id'])!.value,
      prixTotal: this.editForm.get(['prixTotal'])!.value,
      constitueParId: this.editForm.get(['constitueParId'])!.value,
      estValideParId: this.editForm.get(['estValideParId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPanier>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
