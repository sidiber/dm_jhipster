import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPlat } from 'app/shared/model/plat.model';
import { PlatService } from './plat.service';
import { PlatDeleteDialogComponent } from './plat-delete-dialog.component';

@Component({
  selector: 'jhi-plat',
  templateUrl: './plat.component.html',
})
export class PlatComponent implements OnInit, OnDestroy {
  plats?: IPlat[];
  eventSubscriber?: Subscription;

  constructor(
    protected platService: PlatService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.platService.query().subscribe((res: HttpResponse<IPlat[]>) => (this.plats = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPlats();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPlat): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInPlats(): void {
    this.eventSubscriber = this.eventManager.subscribe('platListModification', () => this.loadAll());
  }

  delete(plat: IPlat): void {
    const modalRef = this.modalService.open(PlatDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.plat = plat;
  }
}
