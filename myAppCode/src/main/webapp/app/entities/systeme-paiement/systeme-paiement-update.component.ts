import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISystemePaiement, SystemePaiement } from 'app/shared/model/systeme-paiement.model';
import { SystemePaiementService } from './systeme-paiement.service';

@Component({
  selector: 'jhi-systeme-paiement-update',
  templateUrl: './systeme-paiement-update.component.html',
})
export class SystemePaiementUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    numCarte: [null, [Validators.minLength(16), Validators.maxLength(16)]],
    dateExpiration: [null, [Validators.required]],
    typeCarte: [null, [Validators.maxLength(10)]],
    montant: [],
    dateFacture: [null, [Validators.required]],
  });

  constructor(
    protected systemePaiementService: SystemePaiementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ systemePaiement }) => {
      if (!systemePaiement.id) {
        const today = moment().startOf('day');
        systemePaiement.dateExpiration = today;
        systemePaiement.dateFacture = today;
      }

      this.updateForm(systemePaiement);
    });
  }

  updateForm(systemePaiement: ISystemePaiement): void {
    this.editForm.patchValue({
      id: systemePaiement.id,
      numCarte: systemePaiement.numCarte,
      dateExpiration: systemePaiement.dateExpiration ? systemePaiement.dateExpiration.format(DATE_TIME_FORMAT) : null,
      typeCarte: systemePaiement.typeCarte,
      montant: systemePaiement.montant,
      dateFacture: systemePaiement.dateFacture ? systemePaiement.dateFacture.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const systemePaiement = this.createFromForm();
    if (systemePaiement.id !== undefined) {
      this.subscribeToSaveResponse(this.systemePaiementService.update(systemePaiement));
    } else {
      this.subscribeToSaveResponse(this.systemePaiementService.create(systemePaiement));
    }
  }

  private createFromForm(): ISystemePaiement {
    return {
      ...new SystemePaiement(),
      id: this.editForm.get(['id'])!.value,
      numCarte: this.editForm.get(['numCarte'])!.value,
      dateExpiration: this.editForm.get(['dateExpiration'])!.value
        ? moment(this.editForm.get(['dateExpiration'])!.value, DATE_TIME_FORMAT)
        : undefined,
      typeCarte: this.editForm.get(['typeCarte'])!.value,
      montant: this.editForm.get(['montant'])!.value,
      dateFacture: this.editForm.get(['dateFacture'])!.value
        ? moment(this.editForm.get(['dateFacture'])!.value, DATE_TIME_FORMAT)
        : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISystemePaiement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
