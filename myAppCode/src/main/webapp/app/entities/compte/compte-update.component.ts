import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICompte, Compte } from 'app/shared/model/compte.model';
import { CompteService } from './compte.service';
import { ICooperative } from 'app/shared/model/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/cooperative.service';

@Component({
  selector: 'jhi-compte-update',
  templateUrl: './compte-update.component.html',
})
export class CompteUpdateComponent implements OnInit {
  isSaving = false;
  cooperatives: ICooperative[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    prenom: [null, [Validators.required]],
    email: [
      null,
      [
        Validators.required,
        Validators.pattern('^([a-zA-Z0-9_\\-\\.]+)@(|hotmail|yahoo|imag|gmail|etu.univ-grenoble-alpes|univ-grenoble-alpes+)\\.(fr|com)$'),
      ],
    ],
    categorie: [],
    phoneNumber: [null, [Validators.minLength(10), Validators.maxLength(10)]],
    addressCompte: [],
    codePCompte: [null, [Validators.minLength(5), Validators.maxLength(5)]],
    villeCompte: [],
    membreDeId: [],
  });

  constructor(
    protected compteService: CompteService,
    protected cooperativeService: CooperativeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compte }) => {
      this.updateForm(compte);

      this.cooperativeService.query().subscribe((res: HttpResponse<ICooperative[]>) => (this.cooperatives = res.body || []));
    });
  }

  updateForm(compte: ICompte): void {
    this.editForm.patchValue({
      id: compte.id,
      nom: compte.nom,
      prenom: compte.prenom,
      email: compte.email,
      categorie: compte.categorie,
      phoneNumber: compte.phoneNumber,
      addressCompte: compte.addressCompte,
      codePCompte: compte.codePCompte,
      villeCompte: compte.villeCompte,
      membreDeId: compte.membreDeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const compte = this.createFromForm();
    if (compte.id !== undefined) {
      this.subscribeToSaveResponse(this.compteService.update(compte));
    } else {
      this.subscribeToSaveResponse(this.compteService.create(compte));
    }
  }

  private createFromForm(): ICompte {
    return {
      ...new Compte(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      email: this.editForm.get(['email'])!.value,
      categorie: this.editForm.get(['categorie'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      addressCompte: this.editForm.get(['addressCompte'])!.value,
      codePCompte: this.editForm.get(['codePCompte'])!.value,
      villeCompte: this.editForm.get(['villeCompte'])!.value,
      membreDeId: this.editForm.get(['membreDeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompte>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICooperative): any {
    return item.id;
  }
}
