export const enum Role {
  COURSIER = 'COURSIER',

  COMMERCANT = 'COMMERCANT',

  CLIENT = 'CLIENT',
}
