import { IRestaurant } from 'app/shared/model/restaurant.model';
import { ICourse } from 'app/shared/model/course.model';

export interface IPlat {
  id?: number;
  nomPlat?: string;
  description?: any;
  prix?: string;
  photoContentType?: string;
  photo?: any;
  restaurants?: IRestaurant[];
  courses?: ICourse[];
}

export class Plat implements IPlat {
  constructor(
    public id?: number,
    public nomPlat?: string,
    public description?: any,
    public prix?: string,
    public photoContentType?: string,
    public photo?: any,
    public restaurants?: IRestaurant[],
    public courses?: ICourse[]
  ) {}
}
