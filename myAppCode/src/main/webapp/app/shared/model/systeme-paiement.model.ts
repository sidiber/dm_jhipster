import { Moment } from 'moment';
import { IPanier } from 'app/shared/model/panier.model';

export interface ISystemePaiement {
  id?: number;
  numCarte?: string;
  dateExpiration?: Moment;
  typeCarte?: string;
  montant?: string;
  dateFacture?: Moment;
  paniers?: IPanier[];
}

export class SystemePaiement implements ISystemePaiement {
  constructor(
    public id?: number,
    public numCarte?: string,
    public dateExpiration?: Moment,
    public typeCarte?: string,
    public montant?: string,
    public dateFacture?: Moment,
    public paniers?: IPanier[]
  ) {}
}
