import { Moment } from 'moment';
import { IPlat } from 'app/shared/model/plat.model';
import { EtatCourse } from 'app/shared/model/enumerations/etat-course.model';

export interface ICourse {
  id?: number;
  createdAt?: Moment;
  etat?: EtatCourse;
  startTime?: Moment;
  endTime?: Moment;
  montantPrixTotal?: string;
  montantId?: number;
  plats?: IPlat[];
  livreParNom?: string;
  livreParId?: number;
}

export class Course implements ICourse {
  constructor(
    public id?: number,
    public createdAt?: Moment,
    public etat?: EtatCourse,
    public startTime?: Moment,
    public endTime?: Moment,
    public montantPrixTotal?: string,
    public montantId?: number,
    public plats?: IPlat[],
    public livreParNom?: string,
    public livreParId?: number
  ) {}
}
