import { IPanier } from 'app/shared/model/panier.model';
import { ICourse } from 'app/shared/model/course.model';
import { Role } from 'app/shared/model/enumerations/role.model';

export interface ICompte {
  id?: number;
  nom?: string;
  prenom?: string;
  email?: string;
  categorie?: Role;
  phoneNumber?: string;
  addressCompte?: string;
  codePCompte?: string;
  villeCompte?: string;
  paniers?: IPanier[];
  courses?: ICourse[];
  membreDeNomCoop?: string;
  membreDeId?: number;
}

export class Compte implements ICompte {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public email?: string,
    public categorie?: Role,
    public phoneNumber?: string,
    public addressCompte?: string,
    public codePCompte?: string,
    public villeCompte?: string,
    public paniers?: IPanier[],
    public courses?: ICourse[],
    public membreDeNomCoop?: string,
    public membreDeId?: number
  ) {}
}
