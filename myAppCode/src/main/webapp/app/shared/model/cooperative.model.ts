import { ICompte } from 'app/shared/model/compte.model';

export interface ICooperative {
  id?: number;
  nomCoop?: string;
  villeCoop?: string;
  comptes?: ICompte[];
}

export class Cooperative implements ICooperative {
  constructor(public id?: number, public nomCoop?: string, public villeCoop?: string, public comptes?: ICompte[]) {}
}
