import { IPlat } from 'app/shared/model/plat.model';

export interface IRestaurant {
  id?: number;
  nomResto?: string;
  fraisLivraison?: string;
  adresseResto?: string;
  codePResto?: string;
  villeResto?: string;
  proposes?: IPlat[];
}

export class Restaurant implements IRestaurant {
  constructor(
    public id?: number,
    public nomResto?: string,
    public fraisLivraison?: string,
    public adresseResto?: string,
    public codePResto?: string,
    public villeResto?: string,
    public proposes?: IPlat[]
  ) {}
}
