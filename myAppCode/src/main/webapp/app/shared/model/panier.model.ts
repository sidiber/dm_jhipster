export interface IPanier {
  id?: number;
  prixTotal?: string;
  constitueParNom?: string;
  constitueParId?: number;
  estValideParNumCarte?: string;
  estValideParId?: number;
}

export class Panier implements IPanier {
  constructor(
    public id?: number,
    public prixTotal?: string,
    public constitueParNom?: string,
    public constitueParId?: number,
    public estValideParNumCarte?: string,
    public estValideParId?: number
  ) {}
}
