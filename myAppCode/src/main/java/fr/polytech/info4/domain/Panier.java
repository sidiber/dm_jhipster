package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Panier.
 */
@Entity
@Table(name = "panier")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Panier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "prix_total")
    private String prixTotal;

    @ManyToOne
    @JsonIgnoreProperties(value = "paniers", allowSetters = true)
    private Compte constituePar;

    @ManyToOne
    @JsonIgnoreProperties(value = "paniers", allowSetters = true)
    private SystemePaiement estValidePar;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrixTotal() {
        return prixTotal;
    }

    public Panier prixTotal(String prixTotal) {
        this.prixTotal = prixTotal;
        return this;
    }

    public void setPrixTotal(String prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Compte getConstituePar() {
        return constituePar;
    }

    public Panier constituePar(Compte compte) {
        this.constituePar = compte;
        return this;
    }

    public void setConstituePar(Compte compte) {
        this.constituePar = compte;
    }

    public SystemePaiement getEstValidePar() {
        return estValidePar;
    }

    public Panier estValidePar(SystemePaiement systemePaiement) {
        this.estValidePar = systemePaiement;
        return this;
    }

    public void setEstValidePar(SystemePaiement systemePaiement) {
        this.estValidePar = systemePaiement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Panier)) {
            return false;
        }
        return id != null && id.equals(((Panier) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Panier{" +
            "id=" + getId() +
            ", prixTotal='" + getPrixTotal() + "'" +
            "}";
    }
}
