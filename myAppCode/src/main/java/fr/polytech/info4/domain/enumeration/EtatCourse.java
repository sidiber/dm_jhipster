package fr.polytech.info4.domain.enumeration;

/**
 * The EtatCourse enumeration.
 */
public enum EtatCourse {
    CREE, TRAITEMENT, ANNULEE, FINALISEE
}
