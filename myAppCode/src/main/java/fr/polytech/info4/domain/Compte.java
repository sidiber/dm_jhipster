package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import fr.polytech.info4.domain.enumeration.Role;

/**
 * A Compte.
 */
@Entity
@Table(name = "compte")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Compte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    @NotNull
    @Pattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@(|hotmail|yahoo|imag|gmail|etu.univ-grenoble-alpes|univ-grenoble-alpes+)\\.(fr|com)$")
    @Column(name = "email", nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "categorie")
    private Role categorie;

    @Size(min = 10, max = 10)
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;

    @Column(name = "address_compte")
    private String addressCompte;

    @Size(min = 5, max = 5)
    @Column(name = "code_p_compte", length = 5)
    private String codePCompte;

    @Column(name = "ville_compte")
    private String villeCompte;

    @OneToMany(mappedBy = "constituePar")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Panier> paniers = new HashSet<>();

    @OneToMany(mappedBy = "livrePar")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Course> courses = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "comptes", allowSetters = true)
    private Cooperative membreDe;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Compte nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Compte prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Compte email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getCategorie() {
        return categorie;
    }

    public Compte categorie(Role categorie) {
        this.categorie = categorie;
        return this;
    }

    public void setCategorie(Role categorie) {
        this.categorie = categorie;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Compte phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddressCompte() {
        return addressCompte;
    }

    public Compte addressCompte(String addressCompte) {
        this.addressCompte = addressCompte;
        return this;
    }

    public void setAddressCompte(String addressCompte) {
        this.addressCompte = addressCompte;
    }

    public String getCodePCompte() {
        return codePCompte;
    }

    public Compte codePCompte(String codePCompte) {
        this.codePCompte = codePCompte;
        return this;
    }

    public void setCodePCompte(String codePCompte) {
        this.codePCompte = codePCompte;
    }

    public String getVilleCompte() {
        return villeCompte;
    }

    public Compte villeCompte(String villeCompte) {
        this.villeCompte = villeCompte;
        return this;
    }

    public void setVilleCompte(String villeCompte) {
        this.villeCompte = villeCompte;
    }

    public Set<Panier> getPaniers() {
        return paniers;
    }

    public Compte paniers(Set<Panier> paniers) {
        this.paniers = paniers;
        return this;
    }

    public Compte addPanier(Panier panier) {
        this.paniers.add(panier);
        panier.setConstituePar(this);
        return this;
    }

    public Compte removePanier(Panier panier) {
        this.paniers.remove(panier);
        panier.setConstituePar(null);
        return this;
    }

    public void setPaniers(Set<Panier> paniers) {
        this.paniers = paniers;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public Compte courses(Set<Course> courses) {
        this.courses = courses;
        return this;
    }

    public Compte addCourse(Course course) {
        this.courses.add(course);
        course.setLivrePar(this);
        return this;
    }

    public Compte removeCourse(Course course) {
        this.courses.remove(course);
        course.setLivrePar(null);
        return this;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public Cooperative getMembreDe() {
        return membreDe;
    }

    public Compte membreDe(Cooperative cooperative) {
        this.membreDe = cooperative;
        return this;
    }

    public void setMembreDe(Cooperative cooperative) {
        this.membreDe = cooperative;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Compte)) {
            return false;
        }
        return id != null && id.equals(((Compte) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Compte{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            ", categorie='" + getCategorie() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", addressCompte='" + getAddressCompte() + "'" +
            ", codePCompte='" + getCodePCompte() + "'" +
            ", villeCompte='" + getVilleCompte() + "'" +
            "}";
    }
}
