package fr.polytech.info4.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    COURSIER, COMMERCANT, CLIENT
}
