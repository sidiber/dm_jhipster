package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom_resto")
    private String nomResto;

    @Column(name = "frais_livraison")
    private String fraisLivraison;

    @Column(name = "adresse_resto")
    private String adresseResto;

    @Size(min = 5, max = 5)
    @Column(name = "code_p_resto", length = 5)
    private String codePResto;

    @Column(name = "ville_resto")
    private String villeResto;

    @ManyToMany(mappedBy = "restaurants")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Plat> proposes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomResto() {
        return nomResto;
    }

    public Restaurant nomResto(String nomResto) {
        this.nomResto = nomResto;
        return this;
    }

    public void setNomResto(String nomResto) {
        this.nomResto = nomResto;
    }

    public String getFraisLivraison() {
        return fraisLivraison;
    }

    public Restaurant fraisLivraison(String fraisLivraison) {
        this.fraisLivraison = fraisLivraison;
        return this;
    }

    public void setFraisLivraison(String fraisLivraison) {
        this.fraisLivraison = fraisLivraison;
    }

    public String getAdresseResto() {
        return adresseResto;
    }

    public Restaurant adresseResto(String adresseResto) {
        this.adresseResto = adresseResto;
        return this;
    }

    public void setAdresseResto(String adresseResto) {
        this.adresseResto = adresseResto;
    }

    public String getCodePResto() {
        return codePResto;
    }

    public Restaurant codePResto(String codePResto) {
        this.codePResto = codePResto;
        return this;
    }

    public void setCodePResto(String codePResto) {
        this.codePResto = codePResto;
    }

    public String getVilleResto() {
        return villeResto;
    }

    public Restaurant villeResto(String villeResto) {
        this.villeResto = villeResto;
        return this;
    }

    public void setVilleResto(String villeResto) {
        this.villeResto = villeResto;
    }

    public Set<Plat> getProposes() {
        return proposes;
    }

    public Restaurant proposes(Set<Plat> plats) {
        this.proposes = plats;
        return this;
    }

    public Restaurant addPropose(Plat plat) {
        this.proposes.add(plat);
        plat.getRestaurants().add(this);
        return this;
    }

    public Restaurant removePropose(Plat plat) {
        this.proposes.remove(plat);
        plat.getRestaurants().remove(this);
        return this;
    }

    public void setProposes(Set<Plat> plats) {
        this.proposes = plats;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Restaurant)) {
            return false;
        }
        return id != null && id.equals(((Restaurant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", nomResto='" + getNomResto() + "'" +
            ", fraisLivraison='" + getFraisLivraison() + "'" +
            ", adresseResto='" + getAdresseResto() + "'" +
            ", codePResto='" + getCodePResto() + "'" +
            ", villeResto='" + getVilleResto() + "'" +
            "}";
    }
}
