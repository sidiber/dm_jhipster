package fr.polytech.info4.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cooperative.
 */
@Entity
@Table(name = "cooperative")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Cooperative implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom_coop")
    private String nomCoop;

    @Column(name = "ville_coop")
    private String villeCoop;

    @OneToMany(mappedBy = "membreDe")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Compte> comptes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomCoop() {
        return nomCoop;
    }

    public Cooperative nomCoop(String nomCoop) {
        this.nomCoop = nomCoop;
        return this;
    }

    public void setNomCoop(String nomCoop) {
        this.nomCoop = nomCoop;
    }

    public String getVilleCoop() {
        return villeCoop;
    }

    public Cooperative villeCoop(String villeCoop) {
        this.villeCoop = villeCoop;
        return this;
    }

    public void setVilleCoop(String villeCoop) {
        this.villeCoop = villeCoop;
    }

    public Set<Compte> getComptes() {
        return comptes;
    }

    public Cooperative comptes(Set<Compte> comptes) {
        this.comptes = comptes;
        return this;
    }

    public Cooperative addCompte(Compte compte) {
        this.comptes.add(compte);
        compte.setMembreDe(this);
        return this;
    }

    public Cooperative removeCompte(Compte compte) {
        this.comptes.remove(compte);
        compte.setMembreDe(null);
        return this;
    }

    public void setComptes(Set<Compte> comptes) {
        this.comptes = comptes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cooperative)) {
            return false;
        }
        return id != null && id.equals(((Cooperative) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cooperative{" +
            "id=" + getId() +
            ", nomCoop='" + getNomCoop() + "'" +
            ", villeCoop='" + getVilleCoop() + "'" +
            "}";
    }
}
