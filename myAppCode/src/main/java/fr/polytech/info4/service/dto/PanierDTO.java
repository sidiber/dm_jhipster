package fr.polytech.info4.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.polytech.info4.domain.Panier} entity.
 */
public class PanierDTO implements Serializable {
    
    private Long id;

    private String prixTotal;


    private Long constitueParId;

    private String constitueParNom;

    private Long estValideParId;

    private String estValideParNumCarte;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(String prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Long getConstitueParId() {
        return constitueParId;
    }

    public void setConstitueParId(Long compteId) {
        this.constitueParId = compteId;
    }

    public String getConstitueParNom() {
        return constitueParNom;
    }

    public void setConstitueParNom(String compteNom) {
        this.constitueParNom = compteNom;
    }

    public Long getEstValideParId() {
        return estValideParId;
    }

    public void setEstValideParId(Long systemePaiementId) {
        this.estValideParId = systemePaiementId;
    }

    public String getEstValideParNumCarte() {
        return estValideParNumCarte;
    }

    public void setEstValideParNumCarte(String systemePaiementNumCarte) {
        this.estValideParNumCarte = systemePaiementNumCarte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PanierDTO)) {
            return false;
        }

        return id != null && id.equals(((PanierDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PanierDTO{" +
            "id=" + getId() +
            ", prixTotal='" + getPrixTotal() + "'" +
            ", constitueParId=" + getConstitueParId() +
            ", constitueParNom='" + getConstitueParNom() + "'" +
            ", estValideParId=" + getEstValideParId() +
            ", estValideParNumCarte='" + getEstValideParNumCarte() + "'" +
            "}";
    }
}
