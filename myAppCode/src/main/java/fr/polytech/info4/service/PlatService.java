package fr.polytech.info4.service;

import fr.polytech.info4.service.dto.PlatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.polytech.info4.domain.Plat}.
 */
public interface PlatService {

    /**
     * Save a plat.
     *
     * @param platDTO the entity to save.
     * @return the persisted entity.
     */
    PlatDTO save(PlatDTO platDTO);

    /**
     * Get all the plats.
     *
     * @return the list of entities.
     */
    List<PlatDTO> findAll();

    /**
     * Get all the plats with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<PlatDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" plat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlatDTO> findOne(Long id);

    /**
     * Delete the "id" plat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
