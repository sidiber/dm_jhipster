package fr.polytech.info4.service.mapper;


import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.PanierDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Panier} and its DTO {@link PanierDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompteMapper.class, SystemePaiementMapper.class})
public interface PanierMapper extends EntityMapper<PanierDTO, Panier> {

    @Mapping(source = "constituePar.id", target = "constitueParId")
    @Mapping(source = "constituePar.nom", target = "constitueParNom")
    @Mapping(source = "estValidePar.id", target = "estValideParId")
    @Mapping(source = "estValidePar.numCarte", target = "estValideParNumCarte")
    PanierDTO toDto(Panier panier);

    @Mapping(source = "constitueParId", target = "constituePar")
    @Mapping(source = "estValideParId", target = "estValidePar")
    Panier toEntity(PanierDTO panierDTO);

    default Panier fromId(Long id) {
        if (id == null) {
            return null;
        }
        Panier panier = new Panier();
        panier.setId(id);
        return panier;
    }
}
