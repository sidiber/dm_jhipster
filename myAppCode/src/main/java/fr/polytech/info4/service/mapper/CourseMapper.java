package fr.polytech.info4.service.mapper;


import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.CourseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Course} and its DTO {@link CourseDTO}.
 */
@Mapper(componentModel = "spring", uses = {PanierMapper.class, PlatMapper.class, CompteMapper.class})
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {

    @Mapping(source = "montant.id", target = "montantId")
    @Mapping(source = "montant.prixTotal", target = "montantPrixTotal")
    @Mapping(source = "livrePar.id", target = "livreParId")
    @Mapping(source = "livrePar.nom", target = "livreParNom")
    CourseDTO toDto(Course course);

    @Mapping(source = "montantId", target = "montant")
    @Mapping(target = "removePlat", ignore = true)
    @Mapping(source = "livreParId", target = "livrePar")
    Course toEntity(CourseDTO courseDTO);

    default Course fromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }
}
