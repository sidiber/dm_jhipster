package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import fr.polytech.info4.domain.enumeration.EtatCourse;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Course} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.CourseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /courses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CourseCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EtatCourse
     */
    public static class EtatCourseFilter extends Filter<EtatCourse> {

        public EtatCourseFilter() {
        }

        public EtatCourseFilter(EtatCourseFilter filter) {
            super(filter);
        }

        @Override
        public EtatCourseFilter copy() {
            return new EtatCourseFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter createdAt;

    private EtatCourseFilter etat;

    private InstantFilter startTime;

    private InstantFilter endTime;

    private LongFilter montantId;

    private LongFilter platId;

    private LongFilter livreParId;

    public CourseCriteria() {
    }

    public CourseCriteria(CourseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.etat = other.etat == null ? null : other.etat.copy();
        this.startTime = other.startTime == null ? null : other.startTime.copy();
        this.endTime = other.endTime == null ? null : other.endTime.copy();
        this.montantId = other.montantId == null ? null : other.montantId.copy();
        this.platId = other.platId == null ? null : other.platId.copy();
        this.livreParId = other.livreParId == null ? null : other.livreParId.copy();
    }

    @Override
    public CourseCriteria copy() {
        return new CourseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public EtatCourseFilter getEtat() {
        return etat;
    }

    public void setEtat(EtatCourseFilter etat) {
        this.etat = etat;
    }

    public InstantFilter getStartTime() {
        return startTime;
    }

    public void setStartTime(InstantFilter startTime) {
        this.startTime = startTime;
    }

    public InstantFilter getEndTime() {
        return endTime;
    }

    public void setEndTime(InstantFilter endTime) {
        this.endTime = endTime;
    }

    public LongFilter getMontantId() {
        return montantId;
    }

    public void setMontantId(LongFilter montantId) {
        this.montantId = montantId;
    }

    public LongFilter getPlatId() {
        return platId;
    }

    public void setPlatId(LongFilter platId) {
        this.platId = platId;
    }

    public LongFilter getLivreParId() {
        return livreParId;
    }

    public void setLivreParId(LongFilter livreParId) {
        this.livreParId = livreParId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CourseCriteria that = (CourseCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(etat, that.etat) &&
            Objects.equals(startTime, that.startTime) &&
            Objects.equals(endTime, that.endTime) &&
            Objects.equals(montantId, that.montantId) &&
            Objects.equals(platId, that.platId) &&
            Objects.equals(livreParId, that.livreParId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        createdAt,
        etat,
        startTime,
        endTime,
        montantId,
        platId,
        livreParId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (etat != null ? "etat=" + etat + ", " : "") +
                (startTime != null ? "startTime=" + startTime + ", " : "") +
                (endTime != null ? "endTime=" + endTime + ", " : "") +
                (montantId != null ? "montantId=" + montantId + ", " : "") +
                (platId != null ? "platId=" + platId + ", " : "") +
                (livreParId != null ? "livreParId=" + livreParId + ", " : "") +
            "}";
    }

}
