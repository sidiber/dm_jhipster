package fr.polytech.info4.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import fr.polytech.info4.domain.enumeration.EtatCourse;

/**
 * A DTO for the {@link fr.polytech.info4.domain.Course} entity.
 */
public class CourseDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Instant createdAt;

    private EtatCourse etat;

    @NotNull
    private Instant startTime;

    private Instant endTime;


    private Long montantId;

    private String montantPrixTotal;
    private Set<PlatDTO> plats = new HashSet<>();

    private Long livreParId;

    private String livreParNom;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public EtatCourse getEtat() {
        return etat;
    }

    public void setEtat(EtatCourse etat) {
        this.etat = etat;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Long getMontantId() {
        return montantId;
    }

    public void setMontantId(Long panierId) {
        this.montantId = panierId;
    }

    public String getMontantPrixTotal() {
        return montantPrixTotal;
    }

    public void setMontantPrixTotal(String panierPrixTotal) {
        this.montantPrixTotal = panierPrixTotal;
    }

    public Set<PlatDTO> getPlats() {
        return plats;
    }

    public void setPlats(Set<PlatDTO> plats) {
        this.plats = plats;
    }

    public Long getLivreParId() {
        return livreParId;
    }

    public void setLivreParId(Long compteId) {
        this.livreParId = compteId;
    }

    public String getLivreParNom() {
        return livreParNom;
    }

    public void setLivreParNom(String compteNom) {
        this.livreParNom = compteNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CourseDTO)) {
            return false;
        }

        return id != null && id.equals(((CourseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseDTO{" +
            "id=" + getId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", etat='" + getEtat() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", montantId=" + getMontantId() +
            ", montantPrixTotal='" + getMontantPrixTotal() + "'" +
            ", plats='" + getPlats() + "'" +
            ", livreParId=" + getLivreParId() +
            ", livreParNom='" + getLivreParNom() + "'" +
            "}";
    }
}
