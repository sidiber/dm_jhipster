package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Restaurant} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.RestaurantResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /restaurants?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RestaurantCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomResto;

    private StringFilter fraisLivraison;

    private StringFilter adresseResto;

    private StringFilter codePResto;

    private StringFilter villeResto;

    private LongFilter proposeId;

    public RestaurantCriteria() {
    }

    public RestaurantCriteria(RestaurantCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nomResto = other.nomResto == null ? null : other.nomResto.copy();
        this.fraisLivraison = other.fraisLivraison == null ? null : other.fraisLivraison.copy();
        this.adresseResto = other.adresseResto == null ? null : other.adresseResto.copy();
        this.codePResto = other.codePResto == null ? null : other.codePResto.copy();
        this.villeResto = other.villeResto == null ? null : other.villeResto.copy();
        this.proposeId = other.proposeId == null ? null : other.proposeId.copy();
    }

    @Override
    public RestaurantCriteria copy() {
        return new RestaurantCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomResto() {
        return nomResto;
    }

    public void setNomResto(StringFilter nomResto) {
        this.nomResto = nomResto;
    }

    public StringFilter getFraisLivraison() {
        return fraisLivraison;
    }

    public void setFraisLivraison(StringFilter fraisLivraison) {
        this.fraisLivraison = fraisLivraison;
    }

    public StringFilter getAdresseResto() {
        return adresseResto;
    }

    public void setAdresseResto(StringFilter adresseResto) {
        this.adresseResto = adresseResto;
    }

    public StringFilter getCodePResto() {
        return codePResto;
    }

    public void setCodePResto(StringFilter codePResto) {
        this.codePResto = codePResto;
    }

    public StringFilter getVilleResto() {
        return villeResto;
    }

    public void setVilleResto(StringFilter villeResto) {
        this.villeResto = villeResto;
    }

    public LongFilter getProposeId() {
        return proposeId;
    }

    public void setProposeId(LongFilter proposeId) {
        this.proposeId = proposeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RestaurantCriteria that = (RestaurantCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomResto, that.nomResto) &&
            Objects.equals(fraisLivraison, that.fraisLivraison) &&
            Objects.equals(adresseResto, that.adresseResto) &&
            Objects.equals(codePResto, that.codePResto) &&
            Objects.equals(villeResto, that.villeResto) &&
            Objects.equals(proposeId, that.proposeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomResto,
        fraisLivraison,
        adresseResto,
        codePResto,
        villeResto,
        proposeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RestaurantCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomResto != null ? "nomResto=" + nomResto + ", " : "") +
                (fraisLivraison != null ? "fraisLivraison=" + fraisLivraison + ", " : "") +
                (adresseResto != null ? "adresseResto=" + adresseResto + ", " : "") +
                (codePResto != null ? "codePResto=" + codePResto + ", " : "") +
                (villeResto != null ? "villeResto=" + villeResto + ", " : "") +
                (proposeId != null ? "proposeId=" + proposeId + ", " : "") +
            "}";
    }

}
