package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import fr.polytech.info4.domain.enumeration.Role;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Compte} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.CompteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /comptes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompteCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Role
     */
    public static class RoleFilter extends Filter<Role> {

        public RoleFilter() {
        }

        public RoleFilter(RoleFilter filter) {
            super(filter);
        }

        @Override
        public RoleFilter copy() {
            return new RoleFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nom;

    private StringFilter prenom;

    private StringFilter email;

    private RoleFilter categorie;

    private StringFilter phoneNumber;

    private StringFilter addressCompte;

    private StringFilter codePCompte;

    private StringFilter villeCompte;

    private LongFilter panierId;

    private LongFilter courseId;

    private LongFilter membreDeId;

    public CompteCriteria() {
    }

    public CompteCriteria(CompteCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.prenom = other.prenom == null ? null : other.prenom.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.categorie = other.categorie == null ? null : other.categorie.copy();
        this.phoneNumber = other.phoneNumber == null ? null : other.phoneNumber.copy();
        this.addressCompte = other.addressCompte == null ? null : other.addressCompte.copy();
        this.codePCompte = other.codePCompte == null ? null : other.codePCompte.copy();
        this.villeCompte = other.villeCompte == null ? null : other.villeCompte.copy();
        this.panierId = other.panierId == null ? null : other.panierId.copy();
        this.courseId = other.courseId == null ? null : other.courseId.copy();
        this.membreDeId = other.membreDeId == null ? null : other.membreDeId.copy();
    }

    @Override
    public CompteCriteria copy() {
        return new CompteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNom() {
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public StringFilter getPrenom() {
        return prenom;
    }

    public void setPrenom(StringFilter prenom) {
        this.prenom = prenom;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public RoleFilter getCategorie() {
        return categorie;
    }

    public void setCategorie(RoleFilter categorie) {
        this.categorie = categorie;
    }

    public StringFilter getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(StringFilter phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public StringFilter getAddressCompte() {
        return addressCompte;
    }

    public void setAddressCompte(StringFilter addressCompte) {
        this.addressCompte = addressCompte;
    }

    public StringFilter getCodePCompte() {
        return codePCompte;
    }

    public void setCodePCompte(StringFilter codePCompte) {
        this.codePCompte = codePCompte;
    }

    public StringFilter getVilleCompte() {
        return villeCompte;
    }

    public void setVilleCompte(StringFilter villeCompte) {
        this.villeCompte = villeCompte;
    }

    public LongFilter getPanierId() {
        return panierId;
    }

    public void setPanierId(LongFilter panierId) {
        this.panierId = panierId;
    }

    public LongFilter getCourseId() {
        return courseId;
    }

    public void setCourseId(LongFilter courseId) {
        this.courseId = courseId;
    }

    public LongFilter getMembreDeId() {
        return membreDeId;
    }

    public void setMembreDeId(LongFilter membreDeId) {
        this.membreDeId = membreDeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompteCriteria that = (CompteCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(prenom, that.prenom) &&
            Objects.equals(email, that.email) &&
            Objects.equals(categorie, that.categorie) &&
            Objects.equals(phoneNumber, that.phoneNumber) &&
            Objects.equals(addressCompte, that.addressCompte) &&
            Objects.equals(codePCompte, that.codePCompte) &&
            Objects.equals(villeCompte, that.villeCompte) &&
            Objects.equals(panierId, that.panierId) &&
            Objects.equals(courseId, that.courseId) &&
            Objects.equals(membreDeId, that.membreDeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nom,
        prenom,
        email,
        categorie,
        phoneNumber,
        addressCompte,
        codePCompte,
        villeCompte,
        panierId,
        courseId,
        membreDeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompteCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nom != null ? "nom=" + nom + ", " : "") +
                (prenom != null ? "prenom=" + prenom + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (categorie != null ? "categorie=" + categorie + ", " : "") +
                (phoneNumber != null ? "phoneNumber=" + phoneNumber + ", " : "") +
                (addressCompte != null ? "addressCompte=" + addressCompte + ", " : "") +
                (codePCompte != null ? "codePCompte=" + codePCompte + ", " : "") +
                (villeCompte != null ? "villeCompte=" + villeCompte + ", " : "") +
                (panierId != null ? "panierId=" + panierId + ", " : "") +
                (courseId != null ? "courseId=" + courseId + ", " : "") +
                (membreDeId != null ? "membreDeId=" + membreDeId + ", " : "") +
            "}";
    }

}
