package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Plat} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.PlatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /plats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PlatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomPlat;

    private StringFilter prix;

    private LongFilter restaurantId;

    private LongFilter courseId;

    public PlatCriteria() {
    }

    public PlatCriteria(PlatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nomPlat = other.nomPlat == null ? null : other.nomPlat.copy();
        this.prix = other.prix == null ? null : other.prix.copy();
        this.restaurantId = other.restaurantId == null ? null : other.restaurantId.copy();
        this.courseId = other.courseId == null ? null : other.courseId.copy();
    }

    @Override
    public PlatCriteria copy() {
        return new PlatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomPlat() {
        return nomPlat;
    }

    public void setNomPlat(StringFilter nomPlat) {
        this.nomPlat = nomPlat;
    }

    public StringFilter getPrix() {
        return prix;
    }

    public void setPrix(StringFilter prix) {
        this.prix = prix;
    }

    public LongFilter getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(LongFilter restaurantId) {
        this.restaurantId = restaurantId;
    }

    public LongFilter getCourseId() {
        return courseId;
    }

    public void setCourseId(LongFilter courseId) {
        this.courseId = courseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PlatCriteria that = (PlatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomPlat, that.nomPlat) &&
            Objects.equals(prix, that.prix) &&
            Objects.equals(restaurantId, that.restaurantId) &&
            Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomPlat,
        prix,
        restaurantId,
        courseId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomPlat != null ? "nomPlat=" + nomPlat + ", " : "") +
                (prix != null ? "prix=" + prix + ", " : "") +
                (restaurantId != null ? "restaurantId=" + restaurantId + ", " : "") +
                (courseId != null ? "courseId=" + courseId + ", " : "") +
            "}";
    }

}
