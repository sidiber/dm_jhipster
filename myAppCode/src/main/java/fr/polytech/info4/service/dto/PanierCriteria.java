package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Panier} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.PanierResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /paniers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PanierCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter prixTotal;

    private LongFilter constitueParId;

    private LongFilter estValideParId;

    public PanierCriteria() {
    }

    public PanierCriteria(PanierCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.prixTotal = other.prixTotal == null ? null : other.prixTotal.copy();
        this.constitueParId = other.constitueParId == null ? null : other.constitueParId.copy();
        this.estValideParId = other.estValideParId == null ? null : other.estValideParId.copy();
    }

    @Override
    public PanierCriteria copy() {
        return new PanierCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(StringFilter prixTotal) {
        this.prixTotal = prixTotal;
    }

    public LongFilter getConstitueParId() {
        return constitueParId;
    }

    public void setConstitueParId(LongFilter constitueParId) {
        this.constitueParId = constitueParId;
    }

    public LongFilter getEstValideParId() {
        return estValideParId;
    }

    public void setEstValideParId(LongFilter estValideParId) {
        this.estValideParId = estValideParId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PanierCriteria that = (PanierCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(prixTotal, that.prixTotal) &&
            Objects.equals(constitueParId, that.constitueParId) &&
            Objects.equals(estValideParId, that.estValideParId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        prixTotal,
        constitueParId,
        estValideParId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PanierCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (prixTotal != null ? "prixTotal=" + prixTotal + ", " : "") +
                (constitueParId != null ? "constitueParId=" + constitueParId + ", " : "") +
                (estValideParId != null ? "estValideParId=" + estValideParId + ", " : "") +
            "}";
    }

}
