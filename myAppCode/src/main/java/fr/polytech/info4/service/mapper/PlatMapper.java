package fr.polytech.info4.service.mapper;


import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.PlatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Plat} and its DTO {@link PlatDTO}.
 */
@Mapper(componentModel = "spring", uses = {RestaurantMapper.class})
public interface PlatMapper extends EntityMapper<PlatDTO, Plat> {


    @Mapping(target = "removeRestaurant", ignore = true)
    @Mapping(target = "courses", ignore = true)
    @Mapping(target = "removeCourse", ignore = true)
    Plat toEntity(PlatDTO platDTO);

    default Plat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Plat plat = new Plat();
        plat.setId(id);
        return plat;
    }
}
