export enum EtatCourse {
  CREE = 'CREE',

  TRAITEMENT = 'TRAITEMENT',

  ANNULEE = 'ANNULEE',

  FINALISEE = 'FINALISEE',
}
